//
//  AppDelegate.h
//  Roposo Assignment
//
//  Created by Pulkit on 03/04/16.
//  Copyright © 2016 Roposo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

